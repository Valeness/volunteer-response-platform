# About
The Volunteer Response Platform is a platform that enables volunteers to offer their time to transcribe phone calls and take orders on behalf of individuals who can't make it out on their own during a time of crisis.

# Quickstart
Clone the project, `cp .env.example .env` and run `docker-compose up -d` and then you should be able to access the project at http://localhost:3000
