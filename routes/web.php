<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();
Route::get('/recordings', [
    'middleware' => ['auth'], 'as' => 'recordings', 'uses' => 'RecordingController@index'
]);
Route::post('/recordings/{id}', 'RecordingController@update')->middleware('auth')->name('recordings.update');
Route::get('/recordings/{id}', 'RecordingController@single')->middleware('auth');
