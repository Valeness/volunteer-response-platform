<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StatusSeeder extends Seeder
{

    protected static $STATUSES = [
        0 => 'Transcription',
        1 => 'In Progress',
        2 => 'Delivering',
        3 => 'Delivered',
        4 => 'Cancelled',
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        foreach(self::$STATUSES as $id => $status) {
            DB::insert('INSERT INTO statuses (id, name) VALUES (?, ?)', [$id, $status]);
        }

    }
}
