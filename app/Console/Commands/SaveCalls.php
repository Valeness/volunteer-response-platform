<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Twilio\Rest\Client;

class saveCalls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'command:calls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {

        $sid = Config::get('app.twilio_sid');
        $token = Config::get('app.twilio_token');
        $client = new Client($sid, $token);

        $recordings = $client->recordings->page([], 1);
        while(!empty($recordings)) {
            foreach($recordings as $recording) {
                $call = $client->calls($recording->callSid)->fetch();
                $number = $call->from;
                $recording_uri = $this->transformRecordingUri($recording->uri);
                DB::insert('INSERT INTO calls (twilio_id, "number", recording) VALUES (?, ?, ?) ON CONFLICT DO NOTHING', [$call->sid, $number, $recording_uri]);
            }

            $recordings = $recordings->nextPage();
        }
    }

    private function transformRecordingUri($uri) {

        return str_replace('.json', '.mp3', $uri);
    }

}
