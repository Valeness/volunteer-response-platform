<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Twilio\Rest\Client;

class RecordingController extends Controller
{
    public function index() {
        $recordings = DB::select('SELECT *, c.id as id, s.name as status FROM calls c INNER JOIN statuses s ON s.id = c.status');

        return view('recordings')->with('recordings', $recordings);
    }

    public function single($id) {
        $recordings = DB::select('SELECT c.*, s.name as status, s.id as status_id FROM calls c INNER JOIN statuses s ON s.id = c.status WHERE c.id = ?', [$id]);
        $statuses = DB::select('SELECT * FROM statuses');
        return view('single-recording')->with('recording', $recordings[0])->with('statuses', $statuses);
    }

    public function update(Request $request, $id) {
        $transcription = $request->post('transcription');
        $status = $request->post('status');
        DB::update('UPDATE calls SET transcription = ?, status = ? WHERE id = ?', [$transcription, $status, $id]);

        return redirect('recordings/' . $id);
    }

}
