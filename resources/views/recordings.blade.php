@extends('layouts.app')

@section('content')
    <div class="container flex justify-center mx-auto">
        <div class="w-full max-w-m">
            <table class="table-auto mb-8">
                <thead>
                    <tr>
                        <th class="px-4 py-2">URI</th>
                        <th class="px-4 py-2">Status</th>
                        <th class="px-4 py-2">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($recordings as $key => $recording)
                        <tr class="{{$key&1 ? 'bg-gray-100' : '' }}">
                            <td class="border px-4 py-2">
                                <a href="https://api.twilio.com{{$recording->recording}}">https://api.twilio.com{{$recording->recording}}</a>
                            </td>
                            <td class="border px-4 py-2">
                                {{$recording->status}}
                            </td>
                            <td class="border px-4 py-2">
                                <a class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded" href="/recordings/{{$recording->id}}">
                                    View
                                </a>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>

            <p class="text-center text-gray-500 text-xs">
                &copy;2020 Volunteer Response Platform. All rights reserved.
            </p>
        </div>
    </div>
@endsection
