@extends('layouts.app')

@section('content')
    <div class="container flex justify-center mx-auto">
        <div class="w-full max-w-m">

            <h2>{{$recording->number}}</h2>
            <div style="position: relative;">
                <div id="waveform"></div>

            </div>

            <form class="bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4" method="post" action="{{ route('recordings.update', ['id' => $recording->id]) }}">
                @csrf
                <div class="mb-4">
                    <label for="status">Call Status</label>
                    <select name="status">
                        @foreach($statuses as $status)
                            <option

                                @if($status->id == $recording->status_id)
                                    selected
                                @endif

                                value="{{$status->id}}">{{$status->name}}</option>
                        @endforeach
                    </select>
                    <label class="block text-gray-700 text-sm font-bold mb-2" for="transcription">
                        Recording Transcription
                    </label>
                    <textarea class="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline" id="transcription" type="text" placeholder="Can you pick up some..." name="transcription">{{ $recording->transcription }}</textarea>
                    <div class="flex items-center justify-between">
                        <button class="bg-red-500 hover:bg-red-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline" type="submit">
                            Save
                        </button>
                    </div>

                </div>
            </form>

            <p class="text-center text-gray-500 text-xs">
                &copy;2020 Volunteer Response Platform. All rights reserved.
            </p>
        </div>
    </div>

    <script src="https://unpkg.com/wavesurfer.js"></script>
    <script src="https://unpkg.com/wavesurfer.js/dist/plugin/wavesurfer.cursor.min.js"></script>
    <script>
        console.log(WaveSurfer.Cursor);
        var wavesurfer = WaveSurfer.create({
            container: document.querySelector('#waveform'),
            barWidth: 2,
            barHeight: 1, // the height of the wave
            barGap: null, // the optional spacing between bars of the wave, if not provided will be calculated in legacy format
            plugins: [
                WaveSurfer.cursor.create({
                    showTime: true,
                    opacity: 1,
                    customShowTimeStyle: {
                        'background-color': '#000',
                        color: '#fff',
                        padding: '2px',
                        'font-size': '10px'
                    }
                })
            ]
        });

        wavesurfer.load('https://api.twilio.com{{$recording->recording}}');
        // wavesurfer.on('ready', function () {wavesurfer.play();});

        const log = document.getElementsByName('body');

        var play = true;

        document.addEventListener('keydown', function() {
            if(play) {
                wavesurfer.play();
                play = false;
            } else {
                wavesurfer.pause();
                play = true;
            }
        });


    </script>

@endsection
